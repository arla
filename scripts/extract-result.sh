#!/bin/sh
#
# $Id$
#

if grep 'All test(s) were succesful' $1 > /dev/null; then
    
    echo "	All test(s) were succesful"

else

	T=`grep 'Failed test(s) were:' $1 | sed 's/[^:]*: *//'`

	echo "	Failed test(s) were:"
	echo
	for a in $T ; do
	    echo "	$a"
	done
fi

