dnl $Id$
dnl
dnl Define __EXTENSIONS__
dnl

AC_DEFUN([AC__EXTENSIONS__],[
AH_VERBATIM([__EXTENSIONS__],[
/*
 * Defining this enables us to get the definition of `sigset_t' and
 * other importatnt definitions on Solaris.
 */

#ifndef __EXTENSIONS__
#define __EXTENSIONS__
#endif
])
])
